const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const cwd = process.cwd();
const src = path.resolve(cwd, 'src');
const build = path.resolve(cwd, 'build');


const getEntries = () => {
    let scripts = path.resolve(src, 'scripts');
    let names = fs.readdirSync(scripts);
    let map = {};

    names.forEach((name) => {
        let m = name.match(/(.+)\.js$/);
        let entry = m ? m[1] : '';
        let entryPath = entry ? path.resolve(scripts, name) : '';

        let paths = [];

        paths.push(entryPath);
        if (entry) map[entry] = paths;
    });

    return map;
}

const getOutput = () => {
    return {
        path: build,
        filename: '[name].js',
        chunkFilename: '[id].common.js' // 非入口但又需要打包文件 例如：动态切割代码

    };
}



const addHtmlPluginConf = (config) => {
    let pages = fs.readdirSync(src);
    pages.forEach((filename) => {
        let m = filename.match(/(.+)\.html$/);
        if (m) {
            // @see https://github.com/kangax/html-minifier
            var conf = {
                template: path.resolve(src, filename),
                // @see https://github.com/kangax/html-minifier
                // minify: {
                //     collapseWhitespace: true,
                //     removeComments: true
                // },
                /*
                          title: 用来生成页面的 title 元素
                          filename: 输出的 HTML 文件名，默认是 index.html, 也可以直接配置带有子目录。
                          template: 模板文件路径，支持加载器，比如 html!./index.html
                          inject: true | 'head' | 'body' | false  ,注入所有的资源到特定的 template 或者 templateContent 中，如果设置为 true 或者 body，所有的 javascript 资源将被放置到 body 元素的底部，'head' 将放置到 head 元素中。
                          favicon: 添加特定的 favicon 路径到输出的 HTML 文件中。
                          minify: {} | false , 传递 html-minifier 选项给 minify 输出
                          hash: true | false, 如果为 true, 将添加一个唯一的 webpack 编译 hash 到所有包含的脚本和 CSS 文件，对于解除 cache 很有用。
                          cache: true | false，如果为 true, 这是默认值，仅仅在文件修改之后才会发布文件。
                          showErrors: true | false, 如果为 true, 这是默认值，错误信息会写入到 HTML 页面中
                          chunks: 允许只添加某些块 (比如，仅仅 unit test 块)
                          chunksSortMode: 允许控制块在添加到页面之前的排序方式，支持的值：'none' | 'default' | {function}-default:'auto'
                          excludeChunks: 允许跳过某些块，(比如，跳过单元测试的块) 
                          */
                filename: filename
            };

            if (m[1] in config.entry) {
                conf.inject = 'body';
                conf.chunks = ['common', m[1]];
            }
            config.plugins.push(new HtmlWebpackPlugin(conf)); //打包时候连同html一起打包
        }

    });
}

const addPlugins = (config) => {
    addHtmlPluginConf(config);

}




const config = {
    context: cwd,
    entry: getEntries(),
    output: getOutput(),
    resolve: {
        extensions: ['.js', '.json', '.ejs', '.jsx', '.css', '.scss', '.png', '.jpg', '.less'],
        //开启后缀名的自动补全 默认是.js .json 如果自定义要 '.js','.json' 也要加上
        alias: {
            'jquery': path.resolve(cwd, './node_modules/jquery/src/jquery'),
            '@': path.resolve(src, 'scripts')
        }
    },
    externals: {
        "jQuery": 'jquery',
        $: "jquery"
    },

    module: {
        rules: [{
                test: /\.(tpl|ejs)$/,
                use: 'ejs-loader'
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                include: [src]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 2,
                        minetype: 'images/jpg',
                        name: 'img/[name]_[hash].[ext]' //注意后面的name=xx，这里很重要否则打包后会出现找不到资源的
                    }
                }
            },
            {
                test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?/i,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 8124,
                        name: 'fonts/[name].[ext]'
                    }
                }
            },
            {
                test: /\.html$/,
                use: 'raw-loader'
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ]
};
addPlugins(config)

module.exports = config;