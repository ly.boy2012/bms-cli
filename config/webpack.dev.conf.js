const merge = require('webpack-merge');

const webpack = require('webpack');
const baseWebpackConfig = require('./webpack.base.conf');
const path = require('path');
const cwd = process.cwd();
const build = path.resolve(cwd, '__build')

let webpackConfig = merge(baseWebpackConfig, {
    output: {
        path: build,
        devtoolModuleFilenameTemplate: '[resource-path]',
        sourceMapFilename: '[file].map',
        chunkFilename: '[id].common.js'
    },
    module: {
        rules: [{
            test: /\.css$/,
            use: [{
                    loader: 'style-loader',
                    options: {
                        sourceMap: true
                    }
                },
                {
                    loader: 'css-loader',
                    options: {
                        //importLoaders: 1
                        //minimize: true ,
                        sourceMap: true

                    }
                },
                {
                    loader: 'postcss-loader',
                    options: {
                        sourceMap: 'inline'

                    }
                }
            ]
        }, {
            test: /\.less$/,
            use: [{
                    loader: 'style-loader',
                    options: {
                        sourceMap: true
                    }
                },
                {
                    loader: 'css-loader',
                    options: {
                        //importLoaders: 1
                        //minimize: true ,
                        sourceMap: true

                    }
                },
                {
                    loader: 'less-loader',
                    options: {
                        sourceMap: true
                    }
                }
            ]
        }]
    },
    devtool: '#cheap-module-eval-source-map'


});

webpackConfig.plugins.push(new webpack.DefinePlugin({
    'process.env': {
        NODE_ENV: '"development"'
    }
}));

/*webpackConfig.plugins.push(new webpack.SourceMapDevToolPlugin({
    filename: '[file].map',
    columns: false
}));*/
webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin());
webpackConfig.plugins.push(new webpack.NoEmitOnErrorsPlugin());

module.exports = webpackConfig;