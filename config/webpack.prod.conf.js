const merge = require('webpack-merge');
const webpack = require('webpack')
const baseWebpackConfig = require('./webpack.base.conf');
const conf = require('./config');
const cwd = process.cwd();
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
let webpackConfig = merge(baseWebpackConfig, {
    output: {
        filename: 'scripts/[name].js',
        chunkFilename: 'scripts/[id].common.js'
    },
    module: {
        rules: [{
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [{
                            loader: 'css-loader',
                            options: {
                                //minimize: true ,
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {}
                        }
                    ],
                    publicPath: '../' //定义资源前缀路径 ../img ../fonts
                }),

            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [{
                            loader: 'css-loader',
                            options: {
                                //minimize: true ,
                            }
                        },
                        'less-loader'
                    ],
                    publicPath: '../' //定义资源前缀路径 ../img ../fonts
                }),

            }
        ]
    }
});
var chunks = Object.keys(webpackConfig.entry);
webpackConfig.plugins.push(new ExtractTextPlugin({
    filename: "css/[name].css",
    allChunks: true,
    ignoreOrder: true
}));

webpackConfig.plugins.push(new OptimizeCssAssetsPlugin({
    assetNameRegExp: /\.css$/g,
    cssProcessor: require('cssnano'),
    cssProcessorOptions: { discardComments: { removeAll: true } },
    canPrint: true
}));

webpackConfig.plugins.push(new webpack.optimize.CommonsChunkPlugin({ //提取公用的代码打包到独立文件
    name: 'common',
    chunks: chunks,
    // Modules must be shared between all entries
    minChunks: chunks.length // 提取所有chunks共同依赖的模块
}));
webpackConfig.plugins.push(new webpack.optimize.UglifyJsPlugin({
    compress: {
        warnings: false
    }
}));
webpackConfig.plugins.push(new webpack.DefinePlugin({
    'process.env': {
        NODE_ENV: '"production"'
    }
}));
module.exports = webpackConfig;