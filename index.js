const webpack = require('webpack');
const chalk = require('chalk');
const browserSync = require('browser-sync');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware')
const conf = require('./config/config')
const merge = require('webpack-merge');



const package = require('./package');
const path = require('path');
const fs = require('fs-extra');
const replaceStream = require('replacestream');
const _ = require('lodash');
let program = require('commander');
//const inquirer = require('inquirer');
const cwd = process.cwd();
const template = path.resolve(__dirname, 'templates')


program
    .version(package.version)
    .option('-N, --name [type]', '生成模块名称')
    .option('-T, --tpl [type]', '模板名称')
    .option('-L, --list [type]', '模板列表')
    .option('-D, --dev [type]', '开发调试')
    .option('-B, --build [type]', '编译代码')
    .on('--help', function() {
        console.log('  Examples:');
        console.log('');
        console.log('    $ bms -N user -T template');
        console.log('');
        console.log('  模版列表:');
        console.log('');
        console.log('    $ bms -L');
        console.log('');
    }).parse(process.argv);



if (program.list) {
    list(template)
}

if (program.dev) {
    dev()
}

if (program.build) {
    build()
}

if (program.name && program.tpl) {
    let src = path.resolve(template, program.tpl);
    createModule(src);
}

/**
 * 通过模版创建模块
 * 
 * @param {any} src 
 */
function createModule(src) {
    traversalDir(src);
}
/**
 * 列出当前可用模版
 * 
 * @param {any} uri 
 */
function list(uri) {
    console.log('  模版列表:');
    fs.readdirSync(uri).forEach(file => { //读取文件并遍历
        console.log('  ' + file)
    });
}

function replaceFile(uri, src) {
    let target = _.replace(uri, '{name}', program.name);
    target = _.replace(target, src, cwd);
    //console.log(target);
    fs.ensureFile(target, err => {
        if (!err) {
            fs.createReadStream(uri).pipe(replaceStream('{name}', program.name)).pipe(fs.createWriteStream(target));
        }
    })
}

function traversalDir(uri) {
    let src = path.resolve(template, program.tpl);
    fs.readdirSync(uri).forEach(file => { //读取文件并遍历
        ((fileName, root) => {
            let uri = path.resolve(root, fileName);
            if (fs.lstatSync(uri).isDirectory()) {
                traversalDir(uri); //递归遍历文件
            } else {
                replaceFile(uri, src); //如果是文件做替换
            }
        })(file, uri);

    });
}

function build() {
    const prodConf = require('./config/webpack.prod.conf');
    const localProdConf = require(cwd + '/webpack.prod.conf');
    const newProdConf = merge(prodConf, localProdConf)
    webpack(newProdConf, (err, stats) => {

        if (err) throw err
        process.stdout.write(stats.toString({
            colors: true,
            modules: false,
            children: false,
            chunks: false,
            chunkModules: false
        }) + '\n\n')

        console.log(chalk.green(' 编译完成。\n'))

    });
}

function dev() {
    const devConf = require('./config/webpack.dev.conf');
    const localProdConf = require(cwd + '/webpack.dev.conf');
    const newDevConf = merge(devConf, localProdConf)
    console.log(newDevConf)
    for (var key in newDevConf.entry) {
        newDevConf.entry[key].unshift('webpack-hot-middleware/client?reload=true')
    }
    //console.log(devConf.entry)
    let bundler = webpack(newDevConf);
    console.log(newDevConf.output.path)
    browserSync({
        server: {
            baseDir: newDevConf.output.path,
            //proxy: 'localhost:3000',
            //port: 8080,

            middleware: [
                webpackDevMiddleware(bundler, {
                    noInfo: false,
                    // display no info to console (only warnings and errors)

                    quiet: false,
                    // display nothing to the console

                    lazy: false, //设置为false 才开始实时热更新
                    // switch into lazy mode
                    // that means no watching, but recompilation on every request

                    watchOptions: {
                        aggregateTimeout: 300,
                        poll: true
                    },
                    // watch options (only lazy: false)

                    publicPath: newDevConf.output.publicPath,
                    // public path to bind the middleware to
                    // use the same as in webpack

                    index: "index.html",
                    // the index path for web server

                    headers: { "X-Custom-Header": "yes" },
                    // custom headers

                    stats: {
                        colors: true
                    },
                    // options for formating the statistics

                    reporter: null,
                    // Provide a custom reporter to change the way how logs are shown.

                    serverSideRender: false,
                    // Turn off the server-side rendering mode. See Server-Side Rendering part for more info.
                }),
                webpackHotMiddleware(bundler)

            ]
        },

        // no need to watch '*.js' here, webpack will take care of it for us,
        // including full page reloads if HMR won't work
        files: [
            conf.dev.src + '/*.html'
        ]
    });
}