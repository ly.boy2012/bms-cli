const webpack = require('webpack');
module.exports = {
    plugins: {
        'postcss-import': { addDependencyTo: webpack }, //与css-loader @import 冲突导致url 路径查找错误 option `addDependencyTo` to make the integration with webpack
        'postcss-cssnext': {},
        'postcss-utilities': {},
        'precss': {} //语法参考https://github.com/jonathantneal/precss
    }
}